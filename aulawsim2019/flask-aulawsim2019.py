#!/usr/bin/env python
# coding: utf-8

# In[ ]:


#cemaildb('emaildb')


# In[1]:


import aulawsim2019


# In[ ]:


from flask import Flask, request, jsonify, render_template
#import ic

app = Flask(__name__)


# In[ ]:


@app.route('/createdb', methods=['POST'])
def createdb():
    if request.method == 'POST':
        aulawsim2019.cemaildb()
        aulawsim2019.cuserdb()
        aulawsim2019.cdocdb()
        return('db created')
    #return('do a post request')


# In[ ]:


@app.route('/createuser', methods=['POST'])
def createuser():
    if request.method == 'POST':
        #print (request.is_json)
        jload = request.get_json()
        #return(str(content['team']))
        #email text, password text, 
        #       pubkey text

        email = jload['email']
        password = jload['password']
        pubkey = jload['pubkey']
        
        
        #aulawsim2019.writeemaildb('test', 'hello@gmail.com', 'testsubject', 'thisiscontents',
        #        'text.txt', 'txt, pwn', 'hammersmake@mail.com')
        
        #writeemaildb('team', 'emailaddy', 'subject', 'contents',
        #        'attachment', 'ids', 'emailto')
        aulawsim2019.createuser(email, password, pubkey)
        return('done')
    #return('do a post request')


# In[ ]:


@app.route('/createdoc', methods=['POST'])
def createdoc():
    if request.method == 'POST':
        #print (request.is_json)
        content = request.get_json()
        #return(str(content['team']))

        filename = content['filename']
        contentz = content['content']
        email = content['email']
        
        
        aulawsim2019.writedoc(filename, contentz, email)
        return('done')
    #return('do a post request')


# In[ ]:


#writedocdb(filename, content, ids)


# In[ ]:


@app.route('/sendemail', methods=['POST'])
def sendemail():
    if request.method == 'POST':
        #print (request.is_json)
        content = request.get_json()
        #return(str(content['team']))

        team = content['team']
        email = content['email']
        subject = content['subject']
        contents = content['contents']
        filename = content['filename']
        ids = content['ids']
        emailto = content['emailto']
        
        #aulawsim2019.writeemaildb('test', 'hello@gmail.com', 'testsubject', 'thisiscontents',
        #        'text.txt', 'txt, pwn', 'hammersmake@mail.com')
        
        #writeemaildb('team', 'emailaddy', 'subject', 'contents',
        #        'attachment', 'ids', 'emailto')
        aulawsim2019.writeemail(team, email, subject, contents, filename, ids, emailto)            

        return('done')
    #return('do a post request')


# In[4]:


#import arrow


# In[6]:


#arrow.now().timestamp


# In[2]:


#aulawsim2019.cemaildb()


# In[3]:


#aulawsim2019.writeemaildb('william', 'william.mckee@gmail.com', 
#                          'you are being sued', 'you lose everything',
#                'hello.txt', 345, 'lucy@aulawsim.com')


# In[3]:


#aulawsim2019.readata()


# In[ ]:


@app.route('/getemail', methods=['GET'])
def getemail():
    if request.method == 'GET':
        return(str(aulawsim2019.reademail()))


# In[ ]:


@app.route('/getdoc', methods=['GET'])
def getdoc():
    if request.method == 'GET':
        return(str(aulawsim2019.readoc()))


# In[ ]:


@app.route('/getuser', methods=['GET'])
def getuser():
    if request.method == 'GET':
        return(str(aulawsim2019.readusr()))


# In[1]:





# In[ ]:





# In[ ]:


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5556, debug=True)

