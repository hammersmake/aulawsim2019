#!/usr/bin/env python
# coding: utf-8

# Au Law Sim 2019
# 
# This project was created at disrupting law 2019. 

# In[ ]:


import sqlite3
import arrow
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives.asymmetric import rsa
from cryptography.hazmat.primitives import serialization
from cryptography.hazmat.primitives import hashes
from cryptography.hazmat.primitives.asymmetric import padding
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import serialization


# In[8]:



timenow = arrow.now()

def cemaildb():
        #Creates a database with the table email. 
        
        conn = sqlite3.connect('aulawsim.db')
        c = conn.cursor()
        c.execute('''CREATE TABLE email
                    (team text, email text, 
                    subject text, contents text,
                    datetime text, filename text, 
                    id text, emailto text)''')
        conn.close()


# In[9]:


def cuserdb():
    #Creates database table user.
    conn = sqlite3.connect('aulawsim.db')
    c = conn.cursor()
    c.execute('''CREATE TABLE user
                (email text, password text, 
                pubkey text)''')
    conn.close()


# In[10]:


def cdocdb():
    #Creates database table doc. 
    conn = sqlite3.connect('aulawsim.db')
    c = conn.cursor()
    c.execute('''CREATE TABLE doc
                (filename text, datetime text, 
                content text, email text)''')
    conn.close()


# In[11]:


def keygen(namekey):
    private_key = rsa.generate_private_key(
    public_exponent=65537,
    key_size=2048,
    backend=default_backend())
    public_key = private_key.public_key()
    
    pem = private_key.private_bytes(
    encoding=serialization.Encoding.PEM,
    format=serialization.PrivateFormat.PKCS8,
    encryption_algorithm=serialization.NoEncryption())

    with open('private-{}.pem'.format(namekey), 'wb') as f:
        f.write(pem)
        
    pem = public_key.public_bytes(
    encoding=serialization.Encoding.PEM,
    format=serialization.PublicFormat.SubjectPublicKeyInfo)

    with open('public-{}.pem'.format(namekey), 'wb') as f:
        f.write(pem)
    


# In[12]:


#keygen('testkey')


# In[13]:





# In[14]:


def readkeys(namekey):
    with open("private-{}.pem".format(namekey), "rb") as key_file:
        private_key = serialization.load_pem_private_key(
            key_file.read(),
            password=None,
            backend=default_backend()
        )
    with open("public-{}.pem".format(namekey), "rb") as key_file:
        public_key = serialization.load_pem_public_key(
            key_file.read(),
            backend=default_backend()
        )


# In[18]:


def encryptinfo(message, namekey):
    with open("public-{}.pem".format(namekey), "rb") as key_file:
        public_key = serialization.load_pem_public_key(
            key_file.read(),
            backend=default_backend()
        )
    message = message.encode('utf-8')
    encrypted = public_key.encrypt(
        message,
        padding.OAEP(
            mgf=padding.MGF1(algorithm=hashes.SHA256()),
            algorithm=hashes.SHA256(),
            label=None
        )
    )
    return(encrypted)


# In[20]:


#encryptinfo('this is encrypted', 'testkey')


# In[6]:


#encryptinfo('testkey')


# In[ ]:





# In[ ]:


def createuser(email, password, pubkey):

    conn = sqlite3.connect('aulawsim.db')
    
    c = conn.cursor()
    #timenow = arrow.now()
#email, password, pubkey

    c.execute("""
    INSERT INTO user VALUES (
    '{}','{}', '{}')""".format(email, password, pubkey))

    conn.commit()
    conn.close()


# In[ ]:


def writedoc(filename, content, email):

    conn = sqlite3.connect('aulawsim.db')
    
    c = conn.cursor()
    timenow = arrow.now()


    c.execute("""
    INSERT INTO doc VALUES (
    '{}','{}', '{}', '{}')""".format(filename, timenow.timestamp, content, email))

    conn.commit()
    conn.close()


# In[ ]:





# In[4]:


def writeemail(team, email, subject, contents,
                filename, ids, emailto):
    
    conn = sqlite3.connect('aulawsim.db')
    
    c = conn.cursor()
    timenow = arrow.now()


    c.execute("""
    INSERT INTO email VALUES (
    '{}','{}', '{}', '{}', '{}', '{}', '{}', 
    '{}')""".format(team, email, subject, contents,
                          timenow.timestamp, filename, ids, 
                          emailto))

    conn.commit()
    conn.close()


# In[5]:


#writeemaildb('team', 'email', 'subject', 'contents',
#                'filename', 'ids', 'emailto')


# In[19]:


#readata()


# In[ ]:





# In[20]:


#def reademaildb(emailto):
    


# In[21]:


#cemaildb()


# In[ ]:





# In[22]:


def reademail():
    #This reads the database. Give it name of database.
    conn = sqlite3.connect('aulawsim.db')
    c = conn.cursor()

    c.execute('SELECT * FROM email')
    #conn.close()
    return(c.fetchall())
    conn.close()


# In[23]:


#readata()


# In[24]:


def readoc():
    #This reads the database. Give it name of database.
    conn = sqlite3.connect('aulawsim.db')
    c = conn.cursor()

    c.execute('SELECT * FROM doc')
    #conn.close()
    return(c.fetchall())
    conn.close()


# In[25]:


def readusr():
    #This reads the database. Give it name of database.
    conn = sqlite3.connect('aulawsim.db')
    c = conn.cursor()

    c.execute('SELECT * FROM user')
    #conn.close()
    return(c.fetchall())
    conn.close()


# In[26]:


def createverything(team, email, subject, contents,
                   ids, emailto, filename,
                   content):
    writeemail(team, email, subject, contents,
                attachment, ids, emailto)
    writedoc(filename, content, email)


# In[ ]:


#createverything('myteam', 'will@test.com', 'this is subject',
#               'this is contents of the email', 'hello.txt', )

